#https://github.com/flairNLP/flair/blob/master/resources/docs/TUTORIAL_4_ELMO_BERT_FLAIR_EMBEDDING.md
#https://github.com/flairNLP/flair/blob/1524941783214c1a4a2aa03d064e00e50821e20d/resources/docs/EXPERIMENTS.md
#Sencond link to add word embedding or BERT 

from flair.data import Sentence
from flair.models import SequenceTagger
import pandas as pd
import pickle
import os 
os.chdir('seq2tree/LTL/data/Prepare/Flair/')
from tqdm import tqdm ## tracks progress of loop ##

#additional NLTK tokenise
import nltk
from nltk import word_tokenize





#Load the dataset
df = pd.read_csv('south_src.txt', sep="\n", header=None)

# pd.options.display.max_colwidth = 150 #to view data in full
# text = "get to Blue Room , while avoiding The Ivy Room"

df = df.head(100)
print('NOTE DF HAS BEEN CUT FOR NOW')

#Covert the dataframe into a list of strings
text = df.to_string(index=False, header=False).split('\n')

#Test if worked: 
print('Dataframe:', len(df))
print('Text:', len(text))
# for i in range(10):
#     print(text[i])
#     print('-'*10)


##tag the text with NLTK (additional) 


# load the NER tagger
tagger = SequenceTagger.load('ner-ontonotes')
print(tagger)


#ner is 4 class NER with higher accuracy and ner-ontonotes is a 18 class NER 
# run NER over sentence to identify entities
##method 2:
#f for storing NER tagged string
f_NER = []
#run for every sentence 
for i in tqdm(range(len(text))):
    sentence = Sentence(text[i])
    tagger.predict(sentence)
    #append tagged sentence
    f_NER.append(sentence.to_tagged_string())

#Extension, I think I can add the evaluate here 


##check
print('No: tagged sentences:', len(df))
print('No. input sentences:', len(text))
for i in range(5):
    print(f_NER[i])
    print(text[i])

with open('listfile_flair_ner-ontonotes.txt', 'w') as filehandle:
    for i in f_NER:
        filehandle.write("%s\n" % i)

# tagger.predict(sentence)

# sentence
# print('The following NER tags are found:')

# # iterate over entities and print
# for entity in sentence.get_spans('ner-ontonotes'):
#     print(entity)


# output = sentence.to_tagged_string()
# print(output)
# exit()
# with open('listfile_flair_ner-ontonotes.txt', 'w') as filehandle:
#     filehandle.writelines(output)
#     #for listitem in sentence.to_tagged_string():
#      #   filehandle.write('%s\n' % listitem)
# '''
# output = sentence.to_tagged_string()
# pickle.dump(output, open("flair_output.txt")'''