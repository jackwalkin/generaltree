#extract the data into the desired format 
#train and test to then extract which half for north src 

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from pandas.io.json import json_normalize
from utils import *
import os 
os.chdir('seq2tree/LTL/data/Prepare/Flair/')

from flair.data import Sentence
from flair.models import SequenceTagger
import tqdm ##progress loop##

#Open json and extract elements 
import pprint
import json
data1 = json.load(open('src_nested_space.json', 'r+'))
# print(pprint.pprint(data1))


#get all the list values you need from json
src = entity_swap.extract_element(data1, 'src_id')
tar = entity_swap.extract_element(data1, 'tar_id')
dict_id = entity_swap.extract_element(data1, 'dict_id')
tar_orig = entity_swap.extract_element(data1, 'tar_orig') #to include if you want to do post-processing 

'''for non_ID + additional at the end near save)'''
src = entity_swap.extract_element(data1, 'src_orig')
tar = tar_orig

# ''' '''
#Combine them into a dataframe:
df3 = pd.DataFrame(np.column_stack([src, tar, dict_id, tar_orig]), 
                               columns=['src_id', 'tar_id', 'dict_id','tar_orig'])

# print(df3.head())

#old method before json
'''
#Load data to dataframe
df1 = pd.read_csv('south_src.txt', sep="\n", header=None)
df2 = pd.read_csv('south_tar.txt', sep="\n", header=None)

df3 = pd.merge(df1, df2, left_index=True, right_index=True)
'''

#some of the dataset has a " ." at the end of the string, we want to remove this:
df3['src_id'] = df3['src_id'].str.rstrip(' \n')
df3['tar_id'] = df3['tar_id'].str.lower()
df3['src_id'] = df3['src_id'].str.lower()


#remove rows with undesirable landmarks
# df3 = df3[~df3['0_x'].str.contains("_floor")]

# Split our source and target files with a 20:80 split
train, test = train_test_split(df3, test_size=0.2)

##Substitute some of the testing data with unseen landmarks
#randomly select 50% of testing data 
test = test.sample(frac = 0.5) 

#reset the index to make it easier for extraction (drop=True) means old index is not saved as column
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

# print(train)
# print(test)

#save train and test (half) into corresponding .json file 
#convert this indo a nested dictionary json file so that we can easily access the corresponding dictionary values for each instance
# train.to_json(r'train_unordered.json', orient ='index')
# test.to_json(r'half_test_south.json', orient='index')#save full version at end

# '''comment out from here if you do not want unknown'''
#Load dataset with unseen dataset:
df4 = pd.read_csv('north_some_south_src.txt', sep="\n", header=None)
df5 = pd.read_csv('north_some_south_tar.txt', sep="\n", header=None)

df6 = pd.merge(df4, df5, left_index=True, right_index=True)

#remove rows with undesirable landmarks
# df6 = df6[~df6['0_x'].str.contains("_floor")]
df6 = df6[~df6['0_x'].str.contains("Berk's;Shanghai")]
# print(test.shape)

#Goal: 50% of test with randomly sampled unseen data 
samp = df6.sample(n = int(test.shape[0]))
# print('sample not ratio at the moment')
# samp = df6.sample(n = 20)

#Convert into a list of strings 
text = samp['0_x'].astype(str).values.tolist()
# text = samp['0_x'].to_string(index=False, header=False).split('\n')
targ = samp['0_y'].astype(str).values.tolist()

# with open("north_src.txt", "w") as output:
#     for listitem in text:
#         output.write('%s \n' % listitem)

# with open("north_tar.txt", "w") as output:
#     for listitem in targ:
#         output.write('%s \n' % listitem)


#f for storing NER tagged string of FLAIR
tagger = SequenceTagger.load('ner-ontonotes')
f_NER = []
#run for every sentence 
for i in tqdm.tqdm(range(len(text))):
    sentence = Sentence(text[i])
    tagger.predict(sentence)
    #append tagged sentence
    f_NER.append(sentence.to_tagged_string())


##Preprocessing for entity extraction

#text = source_og
#targ = target 
#f_NER is Flair 
#remove '  .' at end of sentences 
text = [item.rstrip(' .') for item in text]
f_NER = [item.rstrip(' .') for item in f_NER]
#add \n to the end of the string
f_NER = [item + " \n" for item in f_NER]
# remove any spaces that are before the start of the string
text = list(map(str.lstrip, text))
targ = list(map(str.lstrip, targ))
f_NER = list(map(str.lstrip, f_NER))

#Extract FLAIR NER to symbols ($0) with a dictionary
id_sent, key, key_label = entity_swap.__init__(f_NER)
# print(key)
# print(id_sent)



##want target to have corresponding named entities too
#dict + raw --> id form 
tgt_id = entity_swap.__raw_dict_to_id__(targ, key)


# print(tgt_id)
# print('this should all be the same:', len(id_sent), len(tgt_id), len(key), len(targ))


#save as Dataframe
nested = list(zip(id_sent, tgt_id, key, targ))
col=['src_id', 'tar_id', 'dict_id','tar_orig']
north = pd.DataFrame(nested, columns = col)

#add unseen data to test
test = test.append(north, ignore_index=True)
'''comment out up to here if you do not want unknown'''
#remove any \n at end of sentence to keep formatting and lowercase
test['src_id'] = test['src_id'].str.rstrip(' \n')
test['tar_id'] = test['tar_id'].str.lower()
test['src_id'] = test['src_id'].str.lower()

#save test (unordered)
# test.to_json(r'test_unordered.json', orient='index')

#sort by source acending string length
s = train['src_id'].str.len().sort_values().index
train_order = train.reindex(s)
s = test['src_id'].str.len().sort_values().index
test_order = test.reindex(s)
# print(test_order)

#reset index for saving
train_save = train_order.reset_index(drop=True)
test_save = test_order.reset_index(drop=True)

'''only this line for non_id'''
train_save.rename(columns={'src_id': 'src_orig', 'tar_id': 'tar_orig_used'}, inplace=True)
test_save.rename(columns={'src_id': 'src_orig', 'tar_id': 'tar_orig_used'}, inplace=True)


#save order to add back entities later 
train_save.to_json(r'train_orderd_space.json', orient='index')
test_save.to_json(r'test_orderd_space.json', orient='index')

#check:
# data1 = json.load(open('train_orderd.json', 'r+'))
# print(pprint.pprint(data1))
# src = entity_swap.extract_element(data1, 'src_id')
# print(src)

# data1 = json.load(open('test_orderd.json', 'r+'))
# print(pprint.pprint(data1))
# src = entity_swap.extract_element(data1, 'src_id')
# print(src)

#split source and target into individual lists 
train_src = train_order['src_id'].tolist()
train_tgt = train_order['tar_id'].tolist()

test_src = test_order['src_id'].tolist()
test_tgt = test_order['tar_id'].tolist()



### Combine to make source \t target text file
f_out = open("train.txt", "w")

#added with zip
for a, b in zip(train_src, train_tgt):
    f_out.writelines(a+ "	"+b+'\n')
 
    #print(a,"  ", b,'\n')

f_out.close()


y_out = open("test.txt", "w")

#added with zip
for a, b in zip(test_src, test_tgt):
    y_out.writelines(a+ "	"+b+'\n')
 
    #print(a,"  ", b,'\n')

y_out.close()


### Count frequency of the words and symbols - Target
# Create an empty dictionary 
d = dict() 
  
# Loop through each line of the file 
for line in train_tgt: 
    # Remove the leading spaces and newline character 
    line = line.strip() 
  
    # Convert the characters in line to  
    # lowercase to avoid case mismatch 
    # line = line.lower() 
  
    # Split the line into words 
    words = line.split(" ") 
  
    # Iterate over each word in line 
    for word in words: 
        #Only select words with a length
        if len(word) >=1:
            # Check if the word is already in dictionary 
            if word in d: 
                # Increment count of word by 1 
                d[word] = d[word] + 1
            else: 
                # Add the word to dictionary with count 1 
                d[word] = 1
        else:
            continue

#sort the dictionary so that the frequency is in decending order
d_sorted_keys = sorted(d, key=d.get, reverse=True)


# Print the contents of dictionary to txt file 
with open("vocab.f.txt","w") as f:
    for key in d_sorted_keys:
        f.write("{}	{}\n".format(key,d[key]))


### Count frequency of the words and symbols - Source
# Create an empty dictionary 
d = dict() 
  
# Loop through each line of the file 
for line in test_src: 
    # Remove the leading spaces and newline character 
    line = line.strip() 

    # Convert the characters in line to  
    # lowercase to avoid case mismatch 
    line = line.lower() 
  
    # Split the line into words 
    words = line.split(" ") 

    # Iterate over each word in line 
    for word in words: 
        #Only select words with a length
        if len(word) >=1:
            # Check if the word is already in dictionary 
            if word in d: 
                # Increment count of word by 1 
                d[word] = d[word] + 1
            else: 
                # Add the word to dictionary with count 1 
                d[word] = 1
        else:
            continue

#sort the dictionary so that the frequency is in decending order
d_sorted_keys = sorted(d, key=d.get, reverse=True)

# Print the contents of dictionary to txt file 
with open("vocab.q.txt","w") as f:
    for key in d_sorted_keys:
        f.write("{}	{}\n".format(key,d[key]))

##OLD way with txt files 
# input = open("twophrase_south_clean_src.txt", "r")
# output = open("twophrase_south_clean_tar.txt", "r")

# #sort in terms of length of input while keeping the index the same with the output 
# temp = sorted(zip(input, output), key=lambda x: len(x[0]))
# in_list, out_list = map(list, zip(*temp))